ARG R_IMAGE=rocker/r-ver:latest
FROM rocker/r-ver:4.3.1
RUN echo "rocker/r-ver:4.3.1" > docker_image_version
ARG AUTHOR="SK8 team"

LABEL 	org.opencontainers.image.authors="SK8 Team" \
        org.opencontainers.image.licenses="GPL-2.0-or-later" \
      	org.opencontainers.image.source="https://forgemia.inra.fr/sk8/team/ressources/docker/dockerfile" \
      	org.opencontainers.image.vendor="SK8 Project" \
        org.opencontainers.image.description="A simple configuration to run a R-Shiny App with renv"


## Add here libraries dependencies
## there are more libraries that are needed here
## Valeur par defaut de RENV
ARG SYS_LIB=""
## Recuperation de la version de renv en parametre
RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    gdebi-core \
    locales \
    git \
    openssh-client \
    libssh2-1-dev \
    libgit2-dev cmake \
    libsecret-1-dev \
    libpng-dev \
    libnetcdf-dev \
    libicu-dev \
    libgsl0-dev \
    zlib1g-dev \
    make \
    libssl-dev \
    libgdal-dev \
    gdal-bin \
    libproj-dev \
    libgeos-dev \
    libsqlite3-dev \
    git \
    pandoc \
    libudunits2-dev \
    libsodium-dev \
    libcurl4-openssl-dev

## Change timezone
ENV CONTAINER_TIMEZONE Europe/Paris
ENV TZ Europe/Paris

RUN sudo echo "Europe/Paris" > /etc/timezone
RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen fr_FR.UTF-8 \
  && /usr/sbin/update-locale LANG=fr_FR.UTF-8

ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8

## Change shiny user rights
## USER shiny
RUN useradd -ms /bin/bash shiny
RUN passwd shiny -d


## App name by default, the name is set at docker build dynamically using project name
ARG APP_NAME="superApp"
ENV APP_NAME arbocarto-r-app

# copy project into shiny home
RUN mkdir -p /home/shiny/arbocarto-r-app
WORKDIR /home/shiny/arbocarto-r-app
ADD . /home/shiny/arbocarto-r-app

# Valeur par defaut de RENV
ARG RENV_VERSION=0.15.3
# Recuperation de la version de renv en parametre
ENV RENV_VERSION 1.0.3
# renv cache
ARG RENV_PATHS_CACHE="/home/shiny/arbocarto-r-app/.renv_cache"
ENV RENV_PATHS_CACHE ${RENV_PATHS_CACHE}

RUN apt-get update
RUN apt-get -y install default-jre
RUN apt-get -y install default-jdk
RUN R CMD javareconf

## R package dependencies
## Install la derniere version de renv
#RUN Rscript -e "if (!requireNamespace('renv', quietly = TRUE, repos='https//cloud.r-project.org')) install.packages('renv')"
## Install la version de renv utilise
RUN R -e "install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))"
RUN R -e "remotes::install_version('renv',version='1.0.3', repos=c(CRAN = 'https://cloud.r-project.org'))"
RUN Rscript -e "setwd('/home/shiny/arbocarto-r-app'); options(renv.config.cache.symlinks = FALSE); renv::deactivate(); renv::restore()"

## Install R Packages from another sources
## use : https://github.com/r-lib/remotes
## ex:
## RUN -e "remotes::install_gitlab('sk8/sk8-conf/sk8tools@main', host='forgemia.inra.fr')"

## Clean unnecessary libraries
# remove renv cache if any
RUN rm -rf ${RENV_PATHS_CACHE}
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/*


RUN chown -R shiny:shiny /home/shiny

USER shiny

EXPOSE 3838

## App name by default, the name is set at docker build dynamically using project name
ARG APP_VERSION="SK8"
ENV APP_VERSION SK8-1542bf82

CMD ["sh", "-c", "Rscript -e \"shiny::runApp('/home/shiny/arbocarto-r-app', port = 3838, host = '0.0.0.0' )\""]


