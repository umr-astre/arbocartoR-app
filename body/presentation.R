##################################
##################################
### App presentation ###
##################################
##################################

tabItemPresentation <- function() {
  tabItem("PRS",
          img(
            src = 'banner.svg',
            title = "arbocartoR_App_banner",
            width = "100%"
          ),
          br(), br(), br(),
          p(
            "arbocartoR is a web interface, that allows generating simulations from a multi-level model including two different components:
            the deterministic population dynamics of ", em("Aedes")," mosquitoes (", em("albopictus")," and", em("aegypti"),") in various environments and the stochastic dynamics of transmission
            of dengue, zika and chikungunya viruses.
            "),
          p(
            "The underlying model is a spatialized compartmental model considering that mosquitoes and humans reside in independent parcels where their respective densities are assumed to be homogeneous.
            Humans can move between parcels, spatially spreading the diseases.
            Mosquito dynamics are mainly driven by rainfall, temperature, and land use (main covariates identified in the scientific literature).
            The user can modify the importation of viruses by humans, and characterize the implementation of various vector control strategies.
            "),
          p(
            "The tool proposes several synthetic outputs of the simulations.
            The results include the daily dynamics of mosquito populations, and vector/host infections;
            a map displaying the spatialized and temporal dynamics of the R0 over the simulation period and area; and
            the prediction intervals of the number of autochthonous infections and disease spread.
           ")
  )
}
