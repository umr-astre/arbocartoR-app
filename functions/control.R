# reset control data if the box is unchecked
observeEvent(input$control, {
  if(!input$control)
    data$prev_control <- NULL
})

# press Schedule a control action
observeEvent(input$define_ctl, {

  # print(input$sim_period)

  ini_startCtl <- isolate(input$sim_period[1])
  ini_endCtl <- isolate(input$sim_period[2])

  showModal(modalDialog(
    title = "Characterization of the preventive control measure",
    radioButtons("ctrl_action", "Select a measure:",
                 list("Source reduction (removal or destruction of breeding sites)" = "K",
                      "Chemical Larviciding" = "L",
                      "Fogging or Area Spraying (targets adult mosquitoes)" = "A"),
                 inline = FALSE,
                 selected = "K"),
    selectInput(inputId = "Ctl_loc",
                label = "Select where to implemente the measure (by cliking on the map or using the scrolling list below):",
                choices = data$parcels_data$ID
    ),
    numericInput(inputId = 'buffer_width',
                label = "Implement control in a buffer zone (m)",
                min = 0,
                value = 0
                ),
    leafletOutput("map_control"),
    sliderInput("Ctl_effect",
                "Proportion of breeding sites daily removed during the action",
                value = 1,
                min = 1,
                max = 100,
                step = 1,
                post  = " %"),
    dateRangeInput("Ctl_period",
                   "This measure will be implemented from:",
                   start = ini_startCtl, end = ini_endCtl,
                   min = ini_startCtl, max = ini_endCtl),
    br(),
    uiOutput("UI_validatecontrolbutton"),
    DTOutput('display_Control_inmodal'),
    ## This footer replaces the default "Dismiss" button,
    #  which is 'footer = modalButton("Dismiss")'
    footer = actionButton("dismiss_control_modal",label = "Dismiss")
  ))
})

# 'Validate' button

output$UI_validatecontrolbutton <- renderUI({

    validate(
      need(
        all(input$Ctl_period[1] <= input$Ctl_period[2]) &
          all(input$Ctl_period[1] >= input$sim_period[1] %>% as.Date)  &
          all(input$Ctl_period[1] <= input$sim_period[2] %>% as.Date) &
          all(input$Ctl_period[2] >= input$sim_period[1] %>% as.Date) &
          all(input$Ctl_period[2] <= input$sim_period[2] %>% as.Date),
        'Periods of control must be within the simulation period')
    )

  actionButton("add_ctl", label = "Validate")
})

observeEvent(input$dismiss_control_modal,{
  removeModal()
  data$SHAPE_buffer <- NULL
})

# update label effect when changing the selected measure
observeEvent(input$ctrl_action, {
  label_effect <- if(input$ctrl_action == "K") "Proportion of breeding sites daily removed during the action" else
    if(input$ctrl_action == "A") "Additional daily mortality of adults due to action" else
      if(input$ctrl_action == "L") "Additional daily mortality of larvae due to larvicide"

updateSliderInput(
  session = getDefaultReactiveDomain(),
  inputId = "Ctl_effect",
  label = label_effect
)
})

# Map
output$map_control <- renderLeaflet({

    ### add all data to the SHAPE and display

    SHAPE <- data$SHAPE

    SHAPE %<>% st_as_sf
    SHAPE %<>% st_transform(., '+proj=longlat +datum=WGS84')

    map <-leaflet(data = SHAPE) %>%
      addTiles(group = "OSM (default)") %>%
      # Base groups
      addPolygons(group = "ID",
                  color = "#444444",
                  weight = 1,
                  smoothFactor = 0.5,
                  opacity = 1,
                  fillOpacity = 0,
                  fillColor = NULL,
                  layerId = SHAPE$ID,
                  popup =  ~htmlEscape(ID),
                  highlightOptions = highlightOptions(color = "white",
                                                      weight = 2,
                                                      bringToFront = TRUE))



    if(!is.null(data$prev_control)){

      prev_control_by_parcel <- aggregate(p ~ loc, data$prev_control, sum)
      ctldata2plot <- data$SHAPE[data$SHAPE$ID %in% prev_control_by_parcel$loc,]

      ctldata2plot %<>% merge(., prev_control_by_parcel, by.x = "ID", by.y = "loc")

      ctldata2plot %<>% st_as_sf
      ctldata2plot %<>% st_transform(., '+proj=longlat +datum=WGS84')

      map %<>%
        addPolygons(data = ctldata2plot,
                    group = "ID",
                    color = "#76A15C",
                    weight = 1,
                    smoothFactor = 0.5,
                    opacity = 1,
                    fillOpacity = ~p,
                    fillColor = "#76A15C",
                    layerId = ctldata2plot$ID,
                    popup =  ~htmlEscape(ID),
                    highlightOptions = highlightOptions(color = "white", weight = 2,
                                                        bringToFront = TRUE))

    }



if(!is.null(data$SHAPE_buffer)){
  if(is.null(input$Ctl_effect)) opa <- 1 else  opa <- input$Ctl_effect / 100
      map %<>%
        addPolygons(data = data$SHAPE_buffer,
                    group = "ID",
                    color = "#9fbd8c",
                    weight = 1,
                    smoothFactor = 0.5,
                    opacity = 1,
                    fillOpacity = opa,
                    fillColor = "#76A15C",
                    layerId = data$SHAPE_buffer$ID,
                    popup =  ~htmlEscape(ID),
                    highlightOptions = highlightOptions(color = "white", weight = 2,
                                                        bringToFront = TRUE))
      }

    if(!is.null(data$introduction_pts)){
      introSHAPE <- SHAPE[SHAPE$ID %in% data$introduction_pts$dest, ]

      map %<>%
        addPolygons(data = introSHAPE,
                    group = "ID",
                    color = "#f1c232",
                    weight = 1,
                    smoothFactor = 0.5,
                    opacity = 1,
                    # fillOpacity = .8,
                    # fillColor = "#f1c232",
                    layerId = introSHAPE$ID,
                    popup =  ~htmlEscape(ID),
                    highlightOptions = highlightOptions(color = "red", weight = 2,
                                                        bringToFront = TRUE))
    }

  return(map)
})

# Click on the map

observeEvent(input$map_control_shape_click, {

  if(!is.null(input$map_control_shape_click$id))
        updateSelectInput(session, "Ctl_loc",
                          choices = c(data$parcels_data$ID),
                          selected = input$map_control_shape_click$id
        )

  if(input$buffer_width > 0){
    SHAPE_buffer <- relate(data$SHAPE,
                           buffer(data$SHAPE[data$SHAPE$ID == input$Ctl_loc, ],
                                  input$buffer_width),
                           "intersects",
                           pairs=FALSE,
                           na.rm=TRUE) %>% c %>% data$SHAPE[., ] %>% st_as_sf %>% st_transform(., '+proj=longlat +datum=WGS84')


    data$SHAPE_buffer <- SHAPE_buffer
  }

})

observeEvent(list(input$Ctl_loc,
                  input$buffer_width),{

  if(!is.null(input$Ctl_loc) & !is.null(input$buffer_width))
    if(input$buffer_width > 0){
      SHAPE_buffer <- relate(data$SHAPE,
                             buffer(data$SHAPE[data$SHAPE$ID == input$Ctl_loc, ],
                                    input$buffer_width),
                             "intersects",
                             pairs=FALSE,
                             na.rm=TRUE) %>% c %>% data$SHAPE[., ] %>% st_as_sf %>% st_transform(., '+proj=longlat +datum=WGS84')


      data$SHAPE_buffer <- SHAPE_buffer
    }
})


# Update dates to avoid inconsistencies

observeEvent(input$Ctl_period, {
  if(!(NA %in% input$Ctl_period)){
    mu_date_range <- input$Ctl_period

    if(mu_date_range[2] < mu_date_range[1]){
      mu_date_range[2] <- mu_date_range[1]
    }

    updateDateRangeInput(
      session,
      "Ctl_period",
      start = mu_date_range[1],
      end = mu_date_range[2]
    )}
})


# Add control
observeEvent(input$add_ctl,{

  if(input$buffer_width == 0)
    buffer_width <- NULL else
      buffer_width <- input$buffer_width

  data$prev_control <- build_prev_control(action = input$ctrl_action,
                                          loc = input$Ctl_loc,
                                          start = input$Ctl_period[1],
                                          end = input$Ctl_period[2],
                                          p = input$Ctl_effect / 100,
                                          prev_control = data$prev_control,
                                          buffer_width = buffer_width,
                                          SpatVec = data$SHAPE,
                                          plot_buffer = F)

})

# Display control tables

output$display_Control_inmodal <- output$display_Control <- renderDT({

  if(!is.null(data$prev_control)){

    prev_control <- data$prev_control
    prev_control$Delete <- shinyInput(actionButton,
                                      nrow(prev_control),
                                      'delete_',
                                      label = "",icon=icon("trash"),
                                      style = "color: red;background-color: white",
                                      onclick = paste0('Shiny.onInputChange( \"delete_button\" , this.id, {priority: \"event\"})'))

    data$prev_control$ID <- prev_control$ID <- seq.int(from = 1, to = nrow(prev_control), by = 1)

    datatable(prev_control[,!"ID", with=FALSE],
              editable = F,
              filter = "none",
              rownames = F,
              extensions = 'FixedColumns',
              options = list(pageLength = 10,
                             scrollX = TRUE,
                             fixedColumns = list(leftColumns = 2)),
              caption = "K: Source reduction (removal or destruction of breeding sites); L: Chemical Larviciding; A: Fogging or Area Spraying (targets adult mosquitoes)",
              escape=F)
  } else data$prev_control
}
)


observeEvent(input$display_Control_cell_edit, {
  data$prev_control[input$display_Control_cell_edit$row,input$display_Control_cell_edit$col + 1] <- input$display_Control_cell_edit$value
})


# When press delete_button, remove row
observeEvent(input$delete_button, {
  selectedRow <- as.numeric(strsplit(input$delete_button, "_")[[1]][2])
  data$prev_control <<- subset(data$prev_control, ID != selectedRow)

  if(nrow(data$prev_control) == 0) data$prev_control <- NULL
})
