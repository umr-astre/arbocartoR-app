# Downloadable csv of selected dataset ----
output$downloadResults <- downloadHandler(
  filename = function() {
    paste(Sys.Date(),"_arbocartoR_rawresults.rda", sep = "")
  },
  content = function(file) {
    trajectories <- data_output$traj
    save(trajectories, file = file)
  }
)
