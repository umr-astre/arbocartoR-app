output$mapR0 <- renderLeaflet({

  if(!is.null(data_output$traj) & inherits(input$time_output, "Date")){
    daily_traj <-  data_output$traj[[1]][DATE %in% input$time_output,]

    SHAPE <- merge(data_output$SHAPE, daily_traj[, c("ID", "R0")], by = 'ID')
    SHAPE$Risk_R0 <- ifelse(SHAPE$R0 >= 1,
                           "R0 >= 1: epidemic risk",
                           "R0 < 1: negligible epidemic risk") %>% as.factor

    SHAPE %<>% st_as_sf
    SHAPE %<>% st_transform(., '+proj=longlat +datum=WGS84')

    colpal <- colorFactor(c("#D63232", "#76A15C"), levels = c("R0 >= 1: epidemic risk",
                                                        "R0 < 1: negligible epidemic risk"))

    map <- leaflet(data = SHAPE) %>%
      addTiles() %>%
      addPolygons(color = "#444444",
                  weight = 1,
                  smoothFactor = 0.5,
                  opacity = 1.0,
                  fillOpacity = 0.5,
                  fillColor = ~colpal(SHAPE$Risk_R0),
                  layerId = SHAPE$ID,
                  popup =  ~htmlEscape(ID),
                  highlightOptions = highlightOptions(color = "white", weight = 2,
                                                      bringToFront = TRUE)) %>%
      addLegend("bottomright",
                pal = colpal,
                values = ~SHAPE$Risk_R0,
                title = "Basic reproduction number (R0)",
                opacity = 1
      )

    map

  }

})
