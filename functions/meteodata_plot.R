

output$plot_temp <- renderDygraph({
  if(!is.null(data_output$traj)){

    p <- plot_TS(data_output$traj,
            stage = c("temperature", "RR_day"),
            parcels_ids = data_output$selected_parcel,
            simulation = 1)
    p$x$attrs$title <- "Meteorological variations over time and among parcels"
    p$x$attrs$labels <- c("day", "temperature", "rainfall")
    p$x$attrs$colors <- c("#ad0225", "#004c90")

    p %>%
      dyOptions(drawGrid = F) %>%
      dyEvent(input$time_output, input$time_output, labelLoc = "bottom")

  }
})
