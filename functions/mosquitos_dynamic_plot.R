### Select compartment to dysplay dynamics for

output$select_cptm <- renderUI({

  if(!is.null(data_output$introduction_pts)){
    selectizeInput(
      inputId = "select_cptm",
      label = "Display temporal dynamics of:",
      choices = c("Susceptible humans" = "Sh",
                  "Exposed humans" = "Eh",
                  "Infectious humans" = "Ih",
                  "Recovered humans" = "Rh",
                  "Eggs" = "Em",
                  "Larvae" = "Lm",
                  "Pupae" = "Pm",
                  "Emerging adults" = "Aemm",
                  "Host-seeking adult mosquitoes" = "Ahm",
                  "Oviposition-seeking adults mosquitoes" = "Aom",
                  "Exposed mosquitoes" = "EAm",
                  "Infectious mosquitoes" = "IAm",
                  "Infected mosquitoes" = "EIAm",
                  "Adult mosquitoes" = "Am",
                  "Infectious host-seeking adult mosquitoes" = "A2hmI"),
      multiple = TRUE
    )
  } else {
    selectizeInput(
      inputId = "select_cptm",
      label = "Display temporal dynamics of:",
      choices = c("Eggs" = "Em",
                  "Larvae" = "Lm",
                  "Pupae" = "Pm",
                  "Emerging adults" = "Aemm",
                  "Host-seeking adult mosquitoes" = "Ahm",
                  "Oviposition-seeking adults mosquitoes" = "Aom",
                  "Adult mosquitoes" = "Am"),
      multiple = TRUE
    )
  }

})

output$plot_TS <- renderDygraph({

  if(!is.null(data_output$traj) & !is.null(input$select_cptm)){

    data2plot <- data_output$traj
    data2plot %<>% lapply(., function(x){
      cols <- c("DATE", "ID", input$select_cptm)
      x[, ..cols]
    })

    labels <- c("Susceptible humans" = "Sh",
      "Exposed humans" = "Eh",
      "Infectious humans" = "Ih",
      "Recovered humans" = "Rh",
      "Eggs" = "Em",
      "Larvae" = "Lm",
      "Pupae" = "Pm",
      "Emerging adults" = "Aemm",
      "Host-seeking adult mosquitoes" = "Ahm",
      "Oviposition-seeking adults mosquitoes" = "Aom",
      "Exposed mosquitoes" = "EAm",
      "Infectious mosquitoes" = "IAm",
      "Infected mosquitoes" = "EIAm",
      "Adult mosquitoes" = "Am",
      "Infectious host-seeking adult mosquitoes" = "A2hmI")
    labels <- names(labels)[match(input$select_cptm, labels)]

    if(is.null(data_output$traj_baseline)){
    p <- plot_TS(traj = data2plot,
                 stage = input$select_cptm,
                 parcels_ids = data_output$selected_parcel,
                 simulation = 1:length(data2plot))
    p$x$attrs$title <-
      if(!is.null(data_output$selected_parcel))
      paste0("Demography in parcel: ", data_output$selected_parcel, ", over all simulations") else 'Demography over time over all parcels and simulations'
    p$x$attrs$labels <- c("day", labels)

    p %<>%
      dyOptions(drawGrid = F) %>%
      dyEvent(input$time_output, input$time_output, labelLoc = "bottom")

    } else {

      data_baseline <- data_output$traj_baseline
      data_baseline %<>% lapply(., function(x){
        cols <- c("DATE", "ID", input$select_cptm)
        x[, ..cols]
      })

      data_baseline %<>% lapply(., function(x){
        for(oldname in input$select_cptm)
          setnames(x, oldname, paste0(oldname, "_baseline"))
        x
      })

      data2plot <- Map(function(.data2plot, .data_baseline) merge(.data2plot, .data_baseline, by = c('DATE', 'ID')), data2plot, data_baseline)

      p <- plot_TS(traj = data2plot,
                   stage = c(input$select_cptm, paste0(input$select_cptm, "_baseline")),
                   parcels_ids = data_output$selected_parcel,
                   simulation = 1:length(data2plot))

      p$x$attrs$title <-
        if(!is.null(data_output$selected_parcel))
          paste0("Demography in parcel: ", data_output$selected_parcel, ", over all simulations") else 'Demography over time over all parcels and simulations'
      p$x$attrs$labels <- c("day", labels, paste(labels, "without control"))
      p$x$attrs$colors <- c(topo.colors(length(labels)), topo.colors(length(labels)))

      series <- lapply(input$select_cptm, function(x){
        list(axis = "y",
             strokePattern = c(7,3))
      })
      names(series) <- paste(labels, "without control")
      p$x$attrs$series <- series

      p %<>%
        dyOptions(drawGrid = F) %>%
        dyEvent(input$time_output, input$time_output, labelLoc = "bottom")

    }
    return(p)

  }
})
