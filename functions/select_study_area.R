
observeEvent(list(input$use_personal,
                  input$CntFinder), {

# Reset settings when changing study area
                    data$SHAPE <- NULL
                    data$SHAPE_input <- NULL
                    data$parcels_data <- NULL
                    data$meteo_data <- NULL

                    data$prev_control <- NULL
                    data$introduction_pts <- NULL

                    updateCheckboxInput(
                      session = getDefaultReactiveDomain(),
                      inputId = 'introduction',
                      value = FALSE)

                    updateCheckboxInput(
                      session = getDefaultReactiveDomain(),
                      inputId = 'control',
                      value = FALSE)

                    # shinyWidgets package
                    updateCheckboxGroupInput(
                      session,
                      inputId = "CC_display",
                      "Display local variables:",
                      c("Carrying capacity (max nb of larvae)" = "K",
                        "Population" = "POP",
                        "Temperature (°C) from mar. to sep." = "TP_avg",
                        "Accumulated precipitation (mm) from mar. to sep." = "RR_cum"),
                      selected = NULL)

                    # Update data (SHAPE/PARCELS/METEO) when changing country or input data
                    if(input$use_personal){

                      data$parcels_upload %>% setDT
                      data$meteo_upload   %>% setDT

                      data$parcels_upload[, `:=`(Kfix = as.integer(Kfix),
                                                 Kvar = as.integer(Kvar),
                                                 POP  = as.integer(POP))]

                      if("DIFF_ALT" %in% names(data$parcels_upload))
                        data$parcels_upload[, `:=`(DIFF_ALT = as.integer(DIFF_ALT))]
                      if("ALT_M" %in% names(data$parcels_upload))
                        data$parcels_upload[, `:=`(ALT_M = as.integer(ALT_M))]

                      data$meteo_upload[, `:=`(RR = as.numeric(RR),
                                               TP = as.numeric(TP))]

                      data$SHAPE <- data$SHAPE_upload
                      data$parcels_data <- data$parcels_upload
                      data$meteo_data <- data$meteo_upload

                    } else if(input$CntFinder != ""){
                      isolate({
                        load(file = paste0("www/data/",input$CntFinder,"_PARCELS.rda"))

                        if(input$CntFinder == "FRA"){
                          updateSelectizeInput(session,
                                            "DptFinder",
                                            choices = unique(parcels$NAME_DPT),
                                            options = list(
                                              placeholder = 'Please select an option below',
                                              onInitialize = I('function() { this.setValue(""); }')
                          ))
                        } else{
                          data$parcels_data = parcels
                          data$SHAPE <- paste0("www/data/",input$CntFinder,"_SHAPE.shp") %>% vect #st_read
                        }

                        load(file = paste0("www/data/",input$CntFinder,"_METEO.rda"))
                        data$meteo_data = meteo

                      })
                    }
                    if(exists("meteo", inherits = FALSE))
                      rm(meteo)
                    if(exists("parcels", inherits = FALSE))
                      rm(parcels)
                    gc()

                  })


# Update data (SHAPE/PARCELS/METEO) when changing the administrative division
observeEvent(input$DptFinder, {

  if(input$CntFinder != "" & input$DptFinder != ""){

  load(file = paste0("www/data/",input$CntFinder,"_PARCELS.rda"))

    if("NAME_DPT" %in% names(parcels)){

  data$parcels_data = parcels %>% .[NAME_DPT == input$DptFinder, ]

  data$SHAPE <- paste0("www/data/",input$CntFinder,"/",input$CntFinder,"_",substr(input$DptFinder, 1, 2),"_SHAPE.shp") %>% vect %>% .[.$ID %in% data$parcels_data$ID, ]

  data$prev_control <- NULL
  data$introduction_pts <- NULL
  # data$mMov <- NULL

  updateCheckboxInput(
    session = getDefaultReactiveDomain(),
    inputId = 'introduction',
    value = FALSE)

  updateCheckboxInput(
    session = getDefaultReactiveDomain(),
    inputId = 'control',
    value = FALSE)

  # shinyWidgets package
  updateCheckboxGroupInput(
    session,
    inputId = "CC_display",
    "Display local variables:",
    c("Carrying capacity (max nb of larvae)" = "K",
      "Population" = "POP",
      "Temperature (°C) from mar. to sep." = "TP_avg",
      "Accumulated precipitation (mm) from mar. to sep." = "RR_cum"),
    selected = NULL)
}}

})
