output$UIsimdate <- renderUI({
  if(!is.null(data$meteo_data))
  dateRangeInput("sim_period",
                 "Simulation will be run from:",
                 start = min(data$meteo_data$DATE), end = min(min(data$meteo_data$DATE) + years(1), max(data$meteo_data$DATE)), ## lubridate package
                 min = min(data$meteo_data$DATE), max = max(data$meteo_data$DATE),
                 format = "yyyy/mm/dd")
})

observeEvent(input$sim_period, {
  if(!(NA %in% input$sim_period)){
    mu_date_range <- input$sim_period
    if(mu_date_range[1] > mu_date_range[2]){
      mu_date_range[1] <- mu_date_range[2]
    }

    if(mu_date_range[2] < mu_date_range[1]){
      mu_date_range[2] <- mu_date_range[1]
    }

    updateDateRangeInput(
      session,
      "sim_period",
      start = mu_date_range[1],
      end = mu_date_range[2]
    )}

  if(!is.null(data$prev_control))
  data$prev_control %<>% .[
    data$prev_control$start >= mu_date_range[1] &
      data$prev_control$end <= mu_date_range[2],]

  if(!is.null(data$introduction_pts))
  data$introduction_pts %<>% .[
    data$introduction_pts$time >= mu_date_range[1] &
      data$introduction_pts$time <= mu_date_range[2],]

})
