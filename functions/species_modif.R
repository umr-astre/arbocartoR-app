# remove diapause when Ae.aegypti
observeEvent(input$Species, {
  if(input$Species == "Ae. aegypti")
    updateCheckboxInput(session,
                        'diapause',
                        value = FALSE)
})
