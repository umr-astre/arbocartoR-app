output$strip_plot <- renderPlot({

  if(!is.null(data_output$traj_baseline)){
    ninfh = c(lapply(data_output$traj, function(x) x[,max(ninfhL + ninfhE), by = node]$V1 %>% sum) %>% unlist,
              lapply(data_output$traj_baseline, function(x) x[,max(ninfhL + ninfhE), by = node]$V1 %>% sum) %>% unlist)

    if(sum(ninfh) > 0){
    p <- ggplot(data.frame(ninfh = ninfh,
                           strategy = c(rep("with control", length(data_output$traj)),
                                        rep("without control", length(data_output$traj_baseline)))) %>%
                  .[.$ninfh >0,]) +
      geom_jitter(aes(x= ninfh,
                      y= factor(strategy, level = c("without control", "with control")),
                      color=strategy),width=0, height = 0.12, alpha=0.4, show.legend = FALSE) +
      scale_color_manual(values=c("orange", "#D63232")) +
      ylab("") +
      xlab("") +
      labs(title = "Number of secondary cases",
           subtitle = "Strip plot by control strategy") +
      theme_light()

    print(p)
    }
    }

})

