
### Upload parcels
observeEvent(input$fileparcels,{

  file <- input$fileparcels
  ext <- file_ext(file$datapath) ## tools package
  req(file)

  if(is.null(file))
    return(NULL)

  file_contents <- read.table(file$datapath,
                              sep = ";",
                              dec = ".",
                              quote = "",
                              header=TRUE,
                              colClasses = "character")

  required_columns <- c("ID", "DIFF_ALT", "Kfix", "Kvar", "POP")
  column_names <- colnames(file_contents)

  validate(
    need(ext %in% c('xls', 'csv', 'txt', 'xlsx'), message = "Incorrect file type"),
    need(required_columns %in% column_names, message = paste(paste(required_columns[!(required_columns %in% column_names)], "column is missing"), collapse = "\n "))
  )

  data$parcels_upload <- file_contents
})

output$parcels_upload_table <- renderDT({
  data$parcels_upload
})

output$summary_parcel_upload <- renderText({
  if(!is.null(data$parcels_upload)){
    message_to_display <- paste("You have uploaded data for", nrow(data$parcels_upload), "patchs.")

    if(!is.null(data$SHAPE_upload)){

      parcels_shape <- unlist(data$SHAPE_upload[[input$ID_col]])

      if(setdiff(data$parcels_upload$ID,  parcels_shape) %>% length %>% is_greater_than(0))
        message_to_display %<>%  paste(., "\n",
          length(setdiff(data$parcels_upload$ID,
                         parcels_shape)),
              "patchs do not exists in the shapefile IDs.")

      if(setdiff(parcels_shape, data$parcels_upload$ID) %>% length %>% is_greater_than(0))
        message_to_display %<>%  paste(., "\n",
          length(setdiff(parcels_shape,data$parcels_upload$ID)),
                                     "patchs exists in the shapefile but not in the parcel dataset.")

      }

    return(message_to_display)
  }
})
