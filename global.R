# Load and if necessary install packages
# list of packages required
list.of.packages <- c(
                      "shiny",
                      "shinydashboard", "shinyWidgets", "shinyjs", "spsComps", "DT",
                      "arbocartoR",
                      "sf", "terra", "ggplot2", "dygraphs",
                      "leaflet", "htmltools","glue",
                      "ggpubr", "tidyterra", "lubridate", "tools"
                      )

source('functions/check_packages.R', local = TRUE)
check_packages(list.of.packages)

library("shiny")
library("shinydashboard")
library("shinyWidgets")
library("shinyjs")
library("spsComps")
library("DT")
library("arbocartoR")
library("sf")
library("terra")
library("ggplot2")
library("dygraphs")
library("leaflet")
library("htmltools")
library("glue")
library("ggpubr")
library("tidyterra")
library("lubridate")
library("tools")
## only used to import data
# library("KrigR")
# library("geodata")

#### Source function

# App Structure function

source('body/presentation.R', local = TRUE)
source('body/content/tabpanel.R', local = TRUE)
source('body/content/tab1_howto.R', local = TRUE)
source('body/content/tab2_tool.R', local = TRUE)
source('body/content/tab3_import.R', local = TRUE)
source('body/about.R', local = TRUE)

source('header/header_ui.R', local = TRUE)
source('body/sidebar_ui.R', local = TRUE)
source('body/body_ui.R', local = TRUE)

